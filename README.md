<strong>Include a README.txt file in your repo in root directory. The README is your chance to tell us what you have worked on and show us what you have learned. 
Submission with no readme file will be marked ZERO.

<h2>Use the README to:</h2>

Highlight what parts of the app the markers should pay attention to i.e. are your work
You should include a clear statement in the README submitted that describes what YOU have done. 
Make sure you include the above 17 requirements as well as "file name" and "line number" you have addressed each requirement</strong>


<h3>**Requirements: **</h3>
1. It is a Node.js app`<br />`

<br />
2. There is a home page with links to all other pages `<br />`
    1. *The home page is called 'index.html' inside the subdirectory called 'views'*`<br />`
    2. *The links to all the other pages are from line 30-36*`<br />`

<br />
3. There is a register page`<br />`
    1. *The register page is called 'register.html' inside the subdirectory called 'views'*`<br />`
    2. *The get and post requests for this page are in 'main.js' inside the subdirectory called 'routes' from lines 38-89*`<br />`

<br />
4. There is user authentication page (i.e. logins)`<br />`
    1. *There is one user authentication page called 'login.html' inside the subdirectory called 'views'*`<br />`
    2. *The get and post requests for this page are in 'main.js' inside the subdirectory called 'routes' from lines 236-279*`<br />`

<br />
5. There is an add recipe page (available only to logged in users) for each recipe store at least three items: name of the recipe, text of the recipe and the name of the user who created/added the recipe.`<br />`
    1. *The add recipe page is called 'addrecipe.html' inside the subdirectory called 'views'*`<br />`
    2. *Each recipe stores 4 items: Name of the recipe, Ingredients, Description (method) and the Name of the user who added/created it*`<br />`
    3. *The get and post requests for this page are in 'main.js' inside the subdirectory called 'routes' from lines 91-115*`<br />`
    4. *Only logged in users can access this page as seen on line 92 -- the use of 'redirectLogin'* `<br />`

<br />
6. There is an update recipe page (available only to logged in users)`<br />`
    1. *The update recipe page is called 'updaterecipeform.ejs' and 'updaterecipe.ejs' inside the subdirectory called 'views'* `<br />`
    2. *The user needs to enter the name of the recipe in 'updaterecipeform.ejs' and then they get redirected to 'updaterecipe.ejs'*`<br />`
    3. *Each recipe stores 4 items: Name of the recipe, Ingredients, Description (method) and the Name of the user who added/created it*`<br />`
    4. *The form gets populated from the database based on the name of the recipe*`<br />`
    5. *The user can update the ingredients and the method of the recipe* `<br />`
    6. *The get and post requests for this page are in 'main.js' inside the subdirectory called 'routes' from lines 117-179*`<br />`
    7. *Only logged in users can access this page as seen on line 118 -- the use of 'redirectLogin'* `<br />`

<br />
7. There is a delete recipe page (available only to logged in users)`<br />`
    1. *The delete recipe page is called 'deleterecipe.html' inside the subdirectory called 'views'* `<br />`
    2. *The get and post requests for this page are in 'main.js' inside the subdirectory called 'routes' from lines 181-218*`<br />`
    3. *This deletes the entire entry* `<br />`
    4. *Only logged in users can access this page as seen on line 182 -- the use of 'redirectLogin'* `<br />`

<br />
8. There is a list page, listing all recipes and the name of the user who added the recipe `<br />`
    1. *The list page is called 'list.ejs' inside the subdirectory called 'views'* `<br />`
    2. *The get requests for this page are in 'main.js' inside the subdirectory called 'routes' from lines 220-234* `<br />`
    3. *This lists all the available recipes in the database* `<br />`
    4. *It is available to all the users*`<br />`

<br />
9. The forms have some validations`<br />`
    1. *There are form validators that check for email and password are on line 46,47 (inside register.html)*`<br />`
    2. *If there is an error then it will display the error in a json format (line 55)*`<br />`
    3. *The password field has been marked as a required field inside 'register.html' (line 35)*`<br />`

<br />
10. There are useful feedback messages to the user`<br />`
    1. *If there is an error while inputting email and password then it will display the error in a json format (line 55 - inside main.js, /registered route)*`<br />`
    2. *The email field requires an '@' and wold display a message asking you to input it (line 33 - register.html)*`<br />`
    3. *The password field has been marked as a required field inside 'register.html' (line 35) which makes sure that the user will enter this* `<br />`

<br />
11. It has a database backend that implements CRUD operations (the database can be MySQL or Mongodb)`<br />`
    1. *The MongoDB module has been implemented as seen in 'index.js' (no subdirectory) --line 11-18*`<br />`
    2. *It is also being used by* `<br />`
            *GET requests of* `<br />`
                1. */search-result (line 22-35),* `<br />`
                2. */updaterecipeform (line 119-132)*`<br />`
                3. */updaterecipe (line 137-152),* `<br />`
                4. */deleterecipe (line 183-196),*`<br />`
                5. */list (line 222 - 233) ,* `<br />`
                6. */api (line 293 -304)  and* `<br />`
                7. *POST requests of all,* `<br />`
        *as seen in the 'main.js' file (inside the subdirectory called 'routes')* `<br />`
        *the tables have been attached below*`<br />`

<br />
12. The create & update operations take input data from a form or forms (available only to logged in users)`<br />`
    1. *The addrecipe.html file displays a form and collects information to be stored in the database (line 26-32)*`<br />`
    2. *The updaterecipe.ejs file populates the form with the data from the database and then the user can update the form accordingly (line 43-49)* `<br />`
    3. *These two pages are only available to logged in users (line 92 and 118 --- routes/main.js)*`<br />`

<br />
13. The login process uses sessions`<br />`
    1. *Imported the session management module in 'index.js' (line 21-28)*`<br />`
    2. *Stored the session of the current user (line 270) in 'routes/main.js'*`<br />`

<br />
14. Passwords should be stored as hashed
    1. *Password hashing has been done after the user registers in 'routes/main.js' (line 62-87)* `<br />`
    2. *The hashed password gets compared on login as well in 'routes/main.js' (line 243-278)*`<br />`

<br />
15. There is a way to logout`<br />`
    1. *A logout GET request has been that allows the user to logout and then destrys the session* `<br />`
    2. *'routes/mian.js' (line 281-289)*`<br />`

<br />
16. There is a basic api i.e. recipes content can be accessed as json via http method`<br />`
    1. *A basic API has been added and a link to the api page has been added to all the other pages in the web application*`<br />`
    2. *The API route can be seen in 'routes/main.js' (line 291-305)*`<br />`
    3. *It displays all the available recipes in the database in a json format*`<br />`

<br />
17. There are links on all pages to home page providing easy navigation for users`<br />`
    1. *A link to all the pages has been added to the pages that are inside the 'views' folder*`<br />`
    2. *A link to the home page has been added to all the paged irrespective of their route or location*`<br />`

<br />
<h4>Going beyond</h4>
<br />
For some of the requirements you can go beyond the pass mark by going beyond the requirements as follows:
Requirement 6: There is an update recipe page (available only to logged in users)
you can go beyond this requirement by letting ONLY the user who created the same recipe to update it.<br />
    1. *Displaying the recipe's added by the logged in user and allowing only those to be updated*`<br />`
    2. *This can be seen in the get request of /updaterecipeform line 125*<br />

<br />
This is the same for update operation in requirement 12.
Requirement 7: There is an delete recipe page (available only to logged in users)
you can go beyond this requirement by letting ONLY the user who created the same recipe to delete it.<br />
    1. *Have not attempted the extension bit*<br />
    
<br />
You should include your data model or database schema (Entity Relationship diagram) including names of tables or collections and their fields including primary and foreign keys.
If you choose to use Mongodb not SQL make sure your explain your data model in details 
(including the name of the database, collection names, fields in each collection, references from one collection to the other, if there is any) 

<h3>Recipes Table</h3>
|--------------------------------------|<br />
| Attribute  |    Type   |             |<br />
| ---------- | --------- | ----------- |<br />
| id         | object Id | primary key |<br />
| Name       | String    |             |<br />
| Ingredients| String    |             |<br />
|Description | String    |             |<br />
|Author Name | String    | foreign key |<br />
|--------------------------------------|<br />

<h3>Users Table</h3>
|--------------------------------------|<br />
| Attribute  |    Type   |             |<br />
| ---------- | --------- | ----------- |<br />
| id         | object Id | primary key |<br />
| username   | String    | foreign key |<br />
| Password   | String    |             |<br />
| Email      | Email     |             |<br />
|First name  | String    |             |<br />
|Last Name   | String    |             |<br />
|--------------------------------------|<br />

The users table stores the details of the user while registration and the recipes tables stores the details of every recipe. 
The id of the recipe table and the id of the users table are not the same. The username from the users table acts as the Author name in the recipes table. 
<strong>Please switch to the 'Display Source' button to see the deatiled table</strong>

<h3>Bibliography</h3> 

1. https://www.w3schools.com/html/html_form_elements.asp
2. https://www.rapidtables.com/web/color/html-color-codes.html
3. https://www.w3schools.com/tags/tag_textarea.asp
4. https://stackoverflow.com/questions/3580063/how-to-center-a-textarea-using-css
5. https://www.w3schools.com/colors/colors_names.asp
6. https://css-tricks.com/css-basics-styling-links-like-boss/
7. https://www.w3schools.com/css/css3_buttons.asp
8. https://auth0.com/blog/express-validator-tutorial/
9. https://express-validator.github.io/docs/custom-error-messages.html
10. https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/submit
11. https://developer.mongodb.com/quickstart/node-crud-tutorial#node-tutorial-update
12. https://www.npmjs.com/package/express-sanitizer
13. https://www.w3schools.com/nodejs/nodejs_mongodb_delete.asp
14. https://www.w3schools.com/nodejs/nodejs_mongodb_update.asp
15. https://auth0.com/blog/express-validator-tutorial/
16. http://zetcode.com/javascript/mongodb/
<h5>Books</h5><br />
17. Web Development with Node & Express – Ethan Brown (O’Reilly) <br />
18. Express in Action – Evan M. Hahn 
