var session = require ('express-session')
//including the express framework 
var express = require ('express')
var validator = require ('express-validator');

const app = express()
const port = 8000
//sanitizer framework of express
const expressSanitizer = require('express-sanitizer');

//using MongoDB module to manipulate the database 
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost/recipebank";
MongoClient.connect(url, function(err, db) {
	if (err) throw err;
	console.log("Database created!");
	db.close();
});

//added for session management
app.use(session({
	secret: 'somerandomstuffs',
	resave: false,
	saveUninitialized: false,
	cookie: {	
		expires: 600000
	 }
}));

//added for sanitization
app.use(expressSanitizer());
////////////

//body parser handles POST request
var bodyParser= require ('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
//create a simple express server 
require('./routes/main')(app);
app.set('views',__dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
//////////////
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
